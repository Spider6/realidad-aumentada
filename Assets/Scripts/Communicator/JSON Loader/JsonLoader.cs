﻿using System.Collections;
using System;
using System.Reflection;
using UnityEngine;

namespace Communicator.Detail
{
	public class JsonDecoder 
	{
		private ToolUtils tool = new ToolUtils ();

		public T Load<T>(string json) where T : class, new()
		{
            object content = JSON.JsonDecode(json);
			return (T) TryGetValue(content, typeof(T));
		}
        
        private object LoadFromHashtable(Hashtable hashtable, Type type)
		{
			object instance = Activator.CreateInstance(type);
			foreach (FieldInfo field in tool.GetPublicFieldsInfo(type)) 
				TrySetFieldValue (instance, field, hashtable, type);
			return instance;
		}

		private void TrySetFieldValue (object instance, FieldInfo field, Hashtable hashtable, Type type)
		{
			if (hashtable.ContainsKey (field.Name))
				field.SetValue (instance, GetValue (hashtable, field));
			else
				Debug.LogWarning ("Missing field: " + field.Name + " on hashtable for: " + type.Name);
		}

		private object GetValue (Hashtable hashtable, FieldInfo field)
		{
			if (hashtable [field.Name] == null) 
			{
				Debug.LogWarning ("Null value for: " + field.Name);
				return null;
			}
			else
				return GetValueByType (hashtable, field);
		}

		private object GetValueByType (Hashtable hashtable, FieldInfo field)
		{
			return GetValueByType (hashtable [field.Name], field.FieldType);
		}

		private object GetValueByType (object value, Type fieldType)
		{
            if (typeof(IList).IsAssignableFrom (fieldType))
				return GetArray (value, fieldType);
			if (typeof(Double) == value.GetType ())
				return Convert.ChangeType (value, fieldType);
            if (tool.IsACustomClass(fieldType))
                return LoadFromHashtable((Hashtable)value, fieldType);
            return value;
		}
        
		private object GetArray (object value, Type type)
		{
			IList ilist = (IList)Activator.CreateInstance (type);
			Type itemType = type.GetGenericArguments () [0];
			foreach (object item in (IList)value) 
			{
				object converterItem = TryGetValue (item, itemType);
				ilist.Add (converterItem);
			}
			return ilist;
		}	

		private object TryGetValue(object value, Type type)
		{
			if (tool.IsACustomClass(type))
				return LoadFromHashtable ((Hashtable)value, type);
			else
				return GetValueByType (value, type);
		}
	}
}