﻿using System.Collections.Generic;
using System;
using System.Reflection;

namespace Communicator.Detail
{
	public class ToolUtils
	{
		public List<FieldInfo> GetPublicFieldsInfo(Type type) 
		{
			FieldInfo[] fields = type.GetFields();
			List<FieldInfo> publicFields = new List<FieldInfo> (fields);
			return publicFields.FindAll (f => f.IsPublic);
		}

		public bool IsACustomClass(Type type)
		{
			foreach (FieldInfo field in GetPublicFieldsInfo(type)) 
			{
                if (!field.IsStatic)
					return true;
			}
			return false;
		}
	}
}