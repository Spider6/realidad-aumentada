﻿using Communicator.Detail;
using UnityEngine;

namespace Communicator
{
    public class UserDataWrapper 
    {
        private UserDataResponse data;
        
        public string UserName
        {
            get { return data.username; }
        }

        public string City
        {
            get { return data.address.city; }
        }

        public string Street
        {
            get { return data.address.street; }
        }

        public string CompanyName
        {
            get { return data.company.name; }
        }

        public UserDataWrapper(UserDataResponse data)
        {
            this.data = data;
        }
    }
}