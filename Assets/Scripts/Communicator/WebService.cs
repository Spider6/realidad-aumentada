﻿using Communicator.Detail;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Communicator
{
    public class WebService : MonoBehaviour
    {
        public Action<UserDataWrapper> SuccedDelegate;
        public Action<string> FailedDelegate;

        public void GetUsersData()
        {
            StartCoroutine(CallService());
        }
        
        private IEnumerator CallService()
        {
            WWW connector = new WWW("http://jsonplaceholder.typicode.com/users");
            yield return connector;
            if (connector != null)
            {
                if (connector.error == null)
                    OnSucces(connector.text);
                else
                    OnFailed(connector.error);
            }
            connector.Dispose();
            connector = null;
        }

        private void OnFailed(string error)
        {
            FailedDelegate(error);
        }

        private void OnSucces(string response)
        {
            JsonDecoder decoder = new JsonDecoder();
            List<UserDataResponse> users = decoder.Load<List<UserDataResponse>>(response);
            int index = UnityEngine.Random.Range(0, users.Count - 1);
            SuccedDelegate(new UserDataWrapper(users[index]));
        }  
    }
}