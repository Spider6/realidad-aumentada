﻿namespace Communicator.Detail
{
    public class UserDataResponse
    {
        public string username;
        public AddressDataResponse address;
        public CompanyDataResponse company;
    }

    public class AddressDataResponse
    {
        public string street;
        public string city;
    }

    public class CompanyDataResponse
    {
        public string name;
    }
}
