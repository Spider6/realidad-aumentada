﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace UIController
{
    public class GameOver : MonoBehaviour
    {
        public void PlayAgain()
        {
            SceneManager.LoadScene("Start Scene");
        }

        public void Awake()
        {
            gameObject.SetActive(false);
        }
    }
}