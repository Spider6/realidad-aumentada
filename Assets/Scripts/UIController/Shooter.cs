﻿using UnityEngine;
using UIController.InputController;
using System.Collections.Generic;

namespace UIController.Detail
{
    public class Shooter : MonoBehaviour
    {
        [SerializeField]
        private Bullet prefab;
        [SerializeField]
        private Transform content;
        [SerializeField]
        private float delayToNextShoot = 0.5f;

        private float timeToNextShoot;

        private List<Bullet> bullets = new List<Bullet>();
        
        private void Awake()
        {
            for (int i = 0; i < 20; i++)
            {
                Bullet bullet = Instantiate(prefab);
                bullet.Initialize(content);
                bullets.Add(bullet);
            }
        }

        private void Update()
        {
            content.position = transform.position;
            content.rotation = transform.rotation;

            if (InputAccess.Instance.CurrentInput.IsTouching)
                OnTouch();
        }

        private void OnTouch()
        {
            if (timeToNextShoot <= Time.realtimeSinceStartup)
            {
                timeToNextShoot = Time.realtimeSinceStartup + delayToNextShoot;
                Bullet currentBullet = bullets.Find(b => !b.gameObject.activeSelf);
                currentBullet.Shoot();
            }
        }
    }

}
