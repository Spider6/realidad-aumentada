﻿using Communicator;
using UIController.Component;
using UnityEngine;

namespace UIController
{
    public class UserDataController : MonoBehaviour
    {
        [SerializeField]
        private WebService service;
        [SerializeField]
        private GameObject errorContent;
        [SerializeField]
        private GameObject susccesContent;

        public void Awake()
        {
            errorContent.SetActive(false);
            susccesContent.SetActive(false);
            CallService();
        }

        private void CallService()
        {
            service.FailedDelegate += OnFailed;
            service.SuccedDelegate += OnSucced;
            service.GetUsersData();
        }

        private void OnFailed(string error)
        {
            errorContent.SetActive(true);
            TextSetterUtils.SetUp(errorContent, error);
        }

        private void OnSucced(UserDataWrapper userData)
        {
            susccesContent.SetActive(true);
            UserDataSetterUtils.SetUp(susccesContent, userData);
        }
    }

}