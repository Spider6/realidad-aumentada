﻿using Character;
using UnityEngine;

namespace UIController.Detail
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField]
        private Rigidbody myRigidBody;
        [SerializeField]
        private float speed = 100;
        [SerializeField]
        private float lifeTime = 2;

        private float timeToDesactivate;

        public void Initialize(Transform content)
        {
            gameObject.SetActive(false);
            transform.SetParent(content);
            transform.localScale = Vector3.one;
            transform.localRotation = Quaternion.identity;
        }

        public void Shoot()
        {
            timeToDesactivate = Time.realtimeSinceStartup + lifeTime;
            gameObject.SetActive(true);
            myRigidBody.velocity = Vector3.zero;
            myRigidBody.angularVelocity = Vector3.zero;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            myRigidBody.AddForce(transform.forward * speed);
        }

        private void OnTriggerEnter(Collider other)
        {
            CharacterBase character = other.GetComponentInChildren<CharacterBase>(true);
            if (character != null && character.Myfaction == Faction.Enemy)
            {
                character.OnDamage();
                gameObject.SetActive(false);
            }
        }

        private void Update()
        {
            if (timeToDesactivate <= Time.realtimeSinceStartup)
                gameObject.SetActive(false);
        }
    }

}