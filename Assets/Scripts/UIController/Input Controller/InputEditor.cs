using UnityEngine;

namespace UIController.InputController.Detail
{
    public class InputEditor : IInput 
    {
        public bool IsHolding
        {
            get { return Input.GetMouseButton(0); }
        }

        public bool IsStartTouching
        {
            get { return Input.GetMouseButtonDown(0); }
        }

        public bool IsTouching
        {
            get { return IsHolding || IsStartTouching; }
        }

        public Vector2 TouchPosition
        {
            get { return IsTouching ? new Vector2(Input.mousePosition.x, Input.mousePosition.y) : Vector2.zero; }
        }
    }
}