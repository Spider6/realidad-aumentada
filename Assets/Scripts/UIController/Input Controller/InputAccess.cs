using UIController.InputController.Detail;
using UnityEngine;

namespace UIController.InputController
{
    public class InputAccess : MonoBehaviour
    {
        public IInput CurrentInput
        {
            get;
            private set;
        }

        public static InputAccess Instance
        {
            get;
            private set;
        }

        public void Awake()
        {
            Instance = this;

            if(Application.isEditor)
                CurrentInput = new InputEditor();    
            else
                CurrentInput = new InputDevice();    
        }
    }
}