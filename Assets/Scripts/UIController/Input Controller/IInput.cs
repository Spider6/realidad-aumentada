using UnityEngine;

namespace UIController.InputController
{
    public interface IInput
    {
        bool IsTouching { get; }
        bool IsHolding { get; }
        bool IsStartTouching { get; }
        Vector2 TouchPosition { get; }
    }   
}