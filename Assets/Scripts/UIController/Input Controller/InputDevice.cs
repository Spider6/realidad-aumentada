﻿using UnityEngine;

namespace UIController.InputController.Detail
{
    public class InputDevice : IInput 
    {
        public bool IsHolding
        {
            get { return IsStartTouching && (Input.GetTouch(0).phase == TouchPhase.Moved || Input.GetTouch(0).phase == TouchPhase.Stationary); }
        }

        public bool IsStartTouching
        {
            get { return Input.touchCount > 0; }
        }

        public bool IsTouching
        {
            get { return IsHolding || IsStartTouching; }
        }

        public Vector2 TouchPosition
        {
            get { return IsStartTouching ? Input.touches[0].position : Vector2.zero; }
        }
    }
}