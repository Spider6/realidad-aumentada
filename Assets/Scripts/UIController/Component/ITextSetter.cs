﻿using UnityEngine;

namespace UIController.Component
{
    public interface ITextSetter 
    {
        void SetUp(string text);        
    }

    public class TextSetterUtils
    {
        public static void SetUp(GameObject content, string text)
        {
            ITextSetter[] setters = content.GetComponentsInChildren<ITextSetter>(true);
            foreach (ITextSetter setter in setters)
                setter.SetUp(text);
        }
    }
}