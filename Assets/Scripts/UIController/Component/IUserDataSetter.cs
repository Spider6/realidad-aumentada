using Communicator;
using UnityEngine;

namespace UIController.Component
{
    public interface IUserDataSetter
    {
        void SetUp(UserDataWrapper data);
    }

    public class UserDataSetterUtils
    {
        public static void SetUp(GameObject content, UserDataWrapper data)
        {
            IUserDataSetter[] setters = content.GetComponentsInChildren<IUserDataSetter>(true);
            foreach (IUserDataSetter setter in setters)
                setter.SetUp(data);
        }
    }
}