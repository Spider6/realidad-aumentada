using Communicator;
using UIController.Component;

namespace UGUI.Component
{
    public class UserCompanySetter : TextSetter, IUserDataSetter
    {
        public void SetUp(UserDataWrapper data)
        {
            SetUp(data.CompanyName);
        }
    }
}