using Communicator;
using UIController.Component;

namespace UGUI.Component
{
    public class UserStreetSetter : TextSetter, IUserDataSetter
    {
        public void SetUp(UserDataWrapper data)
        {
            SetUp(data.Street);
        }
    }
}