﻿using UIController.Component;
using UnityEngine;
using UnityEngine.UI;

namespace UGUI.Component
{
    public class TextSetter : MonoBehaviour, ITextSetter
    {
        [SerializeField]
        private Text text;

        public virtual void SetUp(string newText)
        {
            if (text == null)
                text = GetComponent<Text>();
            text.text = newText;
        }
    }
}