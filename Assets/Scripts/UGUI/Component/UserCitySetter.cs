using Communicator;
using UIController.Component;

namespace UGUI.Component
{
    public class UserCitySetter : TextSetter, IUserDataSetter
    {
        public void SetUp(UserDataWrapper data)
        {
            SetUp(data.City);
        }
    }
}