using Communicator;
using UIController.Component;

namespace UGUI.Component
{
    public class UserNameSetter : TextSetter, IUserDataSetter
    {
        public void SetUp(UserDataWrapper data)
        {
            SetUp(data.UserName);
        }
    }
}