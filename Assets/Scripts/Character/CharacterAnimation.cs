using System;
using System.Collections;
using UnityEngine;

namespace Character
{
    public class CharacterAnimation : MonoBehaviour
    {
        public event Action DiedDelegate;

        [SerializeField]
        protected Animator animator;
        [SerializeField]
        private ParticleSystem deadParticles;

        public void PlayDamage()
        {
            animator.SetTrigger("Damage");
        }

        public virtual void PlayIdle()
        {
            animator.SetBool("Idle", true);
        }

        public void PlayDead()
        {
            animator.SetBool("Idle", false);
            animator.SetBool("Dead", true);
            deadParticles.Play();
            StartCoroutine(OnEndParticles(deadParticles.duration));
        }

        private IEnumerator OnEndParticles(float time)
        {
            yield return new WaitForSeconds(time);
            if (DiedDelegate != null)
                DiedDelegate();
        }
    }
}