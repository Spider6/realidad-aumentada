using System;

namespace Character
{
    public class CharacterEnemyAnimation : CharacterAnimation
    {
        public event Action AttackedDelegate;

        public void OnAttack()
        {
            if (AttackedDelegate != null)
                AttackedDelegate();
        }

        public override void PlayIdle()
        {
            base.PlayIdle();
            animator.SetBool("Attack", false);
        }

        public void PlayWalk()
        {
            animator.SetBool("Walk", true);
        }

        public void PlayAttack()
        {
            animator.SetBool("Walk", false);
            animator.SetBool("Attack", true);
        }
    }

}