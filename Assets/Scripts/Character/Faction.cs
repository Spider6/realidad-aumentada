namespace Character
{
    public enum Faction
    {
        None = 0,
        Ally = 1,
        Enemy = 2
    }

}