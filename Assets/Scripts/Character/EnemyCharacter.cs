using System;
using UnityEngine;

namespace Character
{
    public class EnemyCharacter : CharacterBase
    {
        [SerializeField]
        private NavMeshAgent agent;
        
        private CharacterBase target;

        private CharacterEnemyAnimation Animator
        {
            get { return (CharacterEnemyAnimation)animator; }
        }

        public override Faction EnemyFaction
        {
            get { return Faction.Ally; }
        }

        public void Initialize(Vector3 target, Transform content, int priority)
        {
            transform.SetParent(content);
            transform.localScale = Vector3.one;
            agent.avoidancePriority = priority;
            agent.destination = target;
            Animator.PlayWalk();
            Animator.AttackedDelegate += OnAttack;
        }
                
        protected override void OnEnemyReach(CharacterBase enemy)
        {
            target = enemy;
            Animator.PlayAttack();
            agent.Stop();
        }

        protected override void OnDead()
        {
            agent.Stop();
            base.OnDead();
        }

        private void OnAttack()
        {
            target.OnDamage();
            if (target.IsDead)
                Animator.PlayIdle();
        }
    }

}