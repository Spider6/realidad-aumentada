﻿using System;
using UnityEngine;

namespace Character
{
    public abstract class CharacterBase : MonoBehaviour
    {
        public event Action<CharacterBase> DiedDelegate;

        [SerializeField]
        protected CharacterAnimation animator;
        [SerializeField]
        private Faction myfaction;
        [SerializeField]
        private int lifes;

        private int restLifes;

        public bool IsDead
        {
            get;
            private set;
        }

        public abstract Faction EnemyFaction
        {
            get;
        }

        public Faction Myfaction
        {
            get { return myfaction; }
        }

        public void OnDamage()
        {
            if (--restLifes < 0)
                OnDead();
            else
                animator.PlayDamage();
        }

        protected abstract void OnEnemyReach(CharacterBase enemy);

        protected virtual void OnDead()
        {
            if(!IsDead)
            {
                IsDead = true;
                animator.PlayDead();
            }
        }

        protected virtual void Awake()
        {
            animator.DiedDelegate += OnDied;
            restLifes = lifes;
            IsDead = false;
        }

        private void OnDied()
        {
            if(DiedDelegate != null)
                DiedDelegate(this);
            Destroy(gameObject);
        }

        private void OnTriggerEnter(Collider other)
        {
            CharacterBase character = other.GetComponentInChildren<CharacterBase>(true);
            if (character != null && character.Myfaction == EnemyFaction)
                OnEnemyReach(character);
        }

    }

}