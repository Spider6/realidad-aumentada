using UnityEngine;

namespace Character
{
    public class AllyCharacter : CharacterBase
    {
        [SerializeField]
        private GameObject gameOverContent;

        public override Faction EnemyFaction
        {
            get { return Faction.None; }
        }

        protected override void OnEnemyReach(CharacterBase enemy)
        {
            
        }

        protected override void OnDead()
        {
            base.OnDead();
            gameOverContent.SetActive(true);
        }

        protected override void Awake()
        {
            base.Awake();
            animator.PlayIdle();
        }
    }

}