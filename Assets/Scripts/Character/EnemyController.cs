using System.Collections.Generic;
using UIController.Component;
using UnityEngine;

namespace Character
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField]
        private List<EnemyCharacter> prefabs;
        [SerializeField]
        private List<Transform> spawnPoints;
        [SerializeField]
        private Transform targetPoint;
        [SerializeField]
        private int initialEnemies = 2;
        [SerializeField]
        private GameObject uiContent;

        private int enemiesDefeated = 0;
        private int currentPriority = 1;

        private void Start()
        {
            for(int i = 0; i < initialEnemies; i++)
                SpawnEnemy();
            UpdateUI();
        }

        private void OnDefeated(CharacterBase enemy)
        {
            enemy.DiedDelegate -= OnDefeated;
            enemiesDefeated ++;
            UpdateUI();
            for (int i = 0; i < (enemiesDefeated / 2); i++)
                SpawnEnemy();
        }

        private void UpdateUI()
        {
            TextSetterUtils.SetUp(uiContent, enemiesDefeated.ToString());
        }

        private void SpawnEnemy ()
        {
            int indexCharacter = Mathf.Abs(Random.Range(1 - prefabs.Count, prefabs.Count - 1));
            int indexSpawn = Random.Range(0, spawnPoints.Count - 1);
            Transform initialPoint = spawnPoints[indexSpawn];
            EnemyCharacter character = (EnemyCharacter)Instantiate(prefabs[indexCharacter], initialPoint.position, initialPoint.rotation);
            character.Initialize(targetPoint.position, transform, currentPriority);
            character.DiedDelegate += OnDefeated;
            currentPriority++;
        }
    }

}